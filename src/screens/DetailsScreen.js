import React, { useState, useEffect } from 'react';
import { Text, StyleSheet } from 'react-native';
import tmdb from '../api/tmdb';

const DetailsScreen = ({ navigation, route }) => {
  const [movie, setMovie] = useState({});

  useEffect(() => {
    getMovie(route.params.id);
  }, []);
  
  async function getMovie(id) {
    try {
      const response = await tmdb.get(`/movie/${id}`, {
        params: {
        }
      })
      setMovie(response.data);
    }
    catch (err) {
      console.log(err);
    }

  }
  return (
    <>
      <Text style={styles.texto}>TÍTULO: {movie.original_title}</Text>
      <Text style={styles.texto}>DATA DE LANÇAMENTO: {movie.release_date}</Text>
      <Text style={styles.texto}>RESUMO: {movie.overview}</Text>
    </>
  )
}

const styles = StyleSheet.create({
  texto: {
      marginBottom:10
  },
});

export default DetailsScreen;
