import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import SearchBar from '../components/SearchBar';
import tmdb from '../api/tmdb';

const HomeScreen = ({navigation}) => {
  const [text, setText] = useState('');
  const [results, setResults] = useState([]);

  const[contentType, setContentType] = useState('movie')

  useEffect(() => {  
    searchTmdb('Fight Club');
  },[]);

  async function searchTmdb(query) {
    try {
      const response = await tmdb.get('/search/movie', {
        params: {
          query,
          include_adult: false,
        }
      })
      setResults(response.data.results);
    }
    catch (err) {
      console.log(err);
    }

  }

  function goToDetailScreen(item){
    if(contentType === 'movie'){
      navigation.navigate("Details", {
        id: item.id
      })
    }else if(contentType === 'tv'){
      navigation.navigate("DetailsTv", {
        id: item.id
      })
    }else{
      navigation.navigate("DetailsPerson", {
        id: item.id
      })
    }
  }

  return (
    <>
      <SearchBar 
        onChangeText={(t) => setText(t)}
        onEndEditing={(t) => searchTmdb(t)}
        value={text}
      />

      <View style={styles.listaOp}>
        <Button title='Movies' style={styles.botao} onPress={()=> setContentType('movie')}/>
        <Button title='TV' style={styles.botao} onPress={()=> setContentType('tv')}/>
        <Button title='PEOPLE' style={styles.botao} onPress={()=> setContentType('person')}/>
      </View>

      <FlatList 
        data={results}
        keyExtractor={item => `${item.id}`}
        renderItem={({ item }) => {
          return(
            <View>
              <TouchableOpacity
                onPress={() => navigation.navigate("Details", {
                  id: item.id
                })}
              >
                <Text>{item.original_title}</Text>
              </TouchableOpacity>
            </View>
          )
        }}
      />
    </>
  )
}

const styles = StyleSheet.create({
  botao: {
    width: 10
  },

  listaOp: {
    display:'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap:10,
    marginBottom:20,
  }
});

export default HomeScreen;
