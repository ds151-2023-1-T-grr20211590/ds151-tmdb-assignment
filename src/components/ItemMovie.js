import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const ItemMovie = (props) => {
    return(
        <Text>{props.item.original_title}</Text>
    );
}

export default ItemMovie;